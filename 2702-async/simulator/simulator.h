
#pragma once

#include "stdint.h"

extern int huart1;

void HAL_Init();
uint32_t HAL_GetTick();
void HAL_Delay(const uint32_t delay);

void HAL_TIM_Base_Start_IT(const int frequency);

uint8_t HAL_UART_GetByte();
void HAL_UART_Receive(void*, uint8_t* p_data, const uint32_t len);
void HAL_UART_Receive_IT(void*, uint8_t* p_data, const uint32_t len);

void SetLed(const uint32_t on);
