
#include "simulator.h"

#include <stdio.h>
uint8_t led_state = 0;

void HAL_TIM_PeriodElapsedCallback()
{
    led_state = !led_state;
    SetLed(led_state);
}

int main()
{
    HAL_Init();

    HAL_TIM_Base_Start_IT(2);

    while (1)
    {
        HAL_Delay(1000);
    }
}
