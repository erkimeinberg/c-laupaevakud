
#if _POSIX_C_SOURCE < 199309L
#   error "POSIX library too old."
#endif

#ifdef __STDC_NO_ATOMICS__
#   error "No atomics available! Make sure you're compiling with --std=c11!"
#endif

#include "simulator.h"

#include <time.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdatomic.h>

int huart1 = 0;

static long long _ms_start;
static pthread_t _timer_thread = 0;

static struct {
    uint8_t msg[6];
    atomic_int msg_iter;
    pthread_t thread_id;
    atomic_bool thread_working;
} _uart_data = {
    .msg = { 'A', 'A', 0, 10, 20, 40 },
    .msg_iter = 0,
    .thread_id = 0
};

typedef struct
{
    pthread_t main_tid;
    uint32_t sleep_time;
} timer_thread_t;

typedef struct
{
    uint8_t* pdata;
    uint32_t len;
    pthread_t main_tid;
} uart_thread_t;

static long long _GetEpochMillis()
{
    struct timespec ts;

    clock_gettime(CLOCK_REALTIME, &ts);

    long long millis = ts.tv_sec * 1000ll;
    millis += ts.tv_nsec / 1000000ll;

    return millis;
}

static void __attribute__((__constructor__)) __Initialize()
{
    _ms_start = _GetEpochMillis();

    atomic_init(&_uart_data.thread_working, false);
    atomic_init(&_uart_data.msg_iter, 0);
}

static void* _TimerThreadFunc(void* d)
{
    timer_thread_t* data = d;

    while (1)
    {
        HAL_Delay(data->sleep_time);
        pthread_kill(data->main_tid, SIGUSR1);
    }

    free(data);
    return NULL;
}

static void* _UartRecvThread(void* d)
{
    uart_thread_t* data = d;

    for (uint32_t i = 0; i < data->len; i++)
    {
        uint8_t gob = HAL_UART_GetByte();
        data->pdata[i] = gob;
    }

    _uart_data.thread_working = false;
    pthread_kill(data->main_tid, SIGUSR2);

    free(data);
}

__attribute__((weak)) void HAL_TIM_PeriodElapsedCallback()
{
    printf("Default timer callback handler.\n");
}

__attribute__((weak)) void HAL_UART_ReceiveCompleteCallback()
{
    printf("Default UART callback handler.\n");
}

void _SigUsr1Handler()
{
    signal(SIGUSR1, _SigUsr1Handler);
    HAL_TIM_PeriodElapsedCallback();
}

void _SigUsr2Handler()
{
    signal(SIGUSR2, _SigUsr2Handler);
    HAL_UART_ReceiveCompleteCallback();
}

void HAL_Init()
{
    signal(SIGUSR1, _SigUsr1Handler);
    signal(SIGUSR2, _SigUsr2Handler);
}

uint32_t HAL_GetTick()
{
    const long long current = _GetEpochMillis();
    const long long diff = current - _ms_start;

    return (uint32_t)diff;
}

void HAL_Delay(const uint32_t delay)
{
    struct timespec ts;
    ts.tv_sec = delay / 1000;
    ts.tv_nsec = (delay % 1000) * 1000000;

    int res;
    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);
}

void HAL_TIM_Base_Start_IT(const int frequency)
{
    timer_thread_t* data = malloc(sizeof(timer_thread_t));

    data->main_tid = pthread_self();
    data->sleep_time = 1000 / frequency;

    pthread_create(&_timer_thread, NULL, _TimerThreadFunc, data);
}

uint8_t HAL_UART_GetByte()
{
    if (_uart_data.msg_iter == 0)
    {
        HAL_Delay(500);
    }

    const uint8_t ret = _uart_data.msg[_uart_data.msg_iter];
    _uart_data.msg_iter = (_uart_data.msg_iter + 1) % sizeof(_uart_data.msg);

    return ret;
}

void HAL_UART_Receive(void* v, uint8_t* p_data, const uint32_t len)
{
    (void)v;

    for (uint32_t recv = 0; recv < len; recv++)
    {
        p_data[recv] = HAL_UART_GetByte();
    }
}

void HAL_UART_Receive_IT(void* v, uint8_t* p_data, const uint32_t len)
{
    bool is_working = _uart_data.thread_working;

    if (is_working)
    {
        printf("You tried to start another receive IT thread while the old one was working! Bad!\n");
        return;
    }

    if (_uart_data.thread_id)
    {
        pthread_join(_uart_data.thread_id, NULL);
    }

    uart_thread_t* to_recv = malloc(sizeof(uart_thread_t));
    to_recv->len = len;
    to_recv->pdata = p_data;
    to_recv->main_tid = pthread_self();

    _uart_data.thread_working = true;
    pthread_create(&_uart_data.thread_id, NULL, _UartRecvThread, to_recv);
}

void SetLed(const uint32_t on)
{
    if (on)
        printf("LED is ON\n");
    else
        printf("LED is OFF\n");
}
